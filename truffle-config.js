var HDWalletProvider = require("truffle-hdwallet-provider");

module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // for more about customizing your Truffle configuration!
  networks: {
    development: {
      host: "127.0.0.1",
      port: 7545,
      network_id: "*" // Match any network id
    },
    cloud: {
      host: "ipfs-hugbunters.braindrop.be",
      port: 8545,
      network_id:"*"
    },
    goerli: {
      provider: () => {
        return new HDWalletProvider('river theory observe patient own shallow track kingdom runway stable quantum hard', 'https://goerli.infura.io/v3/' + '696b339b5cd740f9b9ffd8bd022fc4fd')
      },
      network_id: '5', // eslint-disable-line camelcase
      gas: 4465030,
      gasPrice: 10000000000,
    },
  }
};
