# hugbunters-ethereum

## Introduction

This project includes the ethereum smart contracts for hugbunters.

## Preparation

* Set up a development ethereum blockchain by installing ![Ganache](https://www.trufflesuite.com/ganache).

## Installation
This project uses truffle for smart contract development. Install truffle by using:
```
npm install -g truffle
```

## Usage
An example on the usage can be found ![here](https://www.trufflesuite.com/tutorials/pet-shop)

To compile the smart contracts:
```
truffle compile
```

To migrate and deploy the smart contracts to the goerli network, use the following command:
```
truffle migrate --network goerli --reset
```

To execute the tests:
```
truffle test
```
