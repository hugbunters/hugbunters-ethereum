pragma solidity ^0.5.0;
pragma experimental ABIEncoderV2;

contract Bunts {

  //enum BuntType { Typo, Bug, Outdated, Other }
  struct Bunt {
    uint16 id;
    address reporter;
    uint timestamp;
    uint8 buntType;
    string url;
    string description;
    string screenshot_cid;
  }

  //Events
  event BuntCreated(Bunt bunt);

  //Storage
  Bunt[] bunts;
  uint16 idCounter=2000;

	// Register a Bunt
	function register(uint timestamp, uint8 buntType, string memory url, string memory description, string memory screenshot_cid) public returns (bool) {
      address reporter =  msg.sender;
      bunts.push(Bunt(++idCounter, reporter, timestamp, buntType, url, description, screenshot_cid));

      return true;
	}

	// Retrieving the adopters
	function getBunts(uint number) public view returns (Bunt[] memory) {
    if(number < bunts.length) {
      uint firstElement = bunts.length - number;
      Bunt[] memory result = new Bunt[](number);
      uint resultIndex = 0;
      for(uint i=firstElement ; i< bunts.length ; i++) {
        result[resultIndex++]= bunts[i];
      }
      return result;
    } else {
      return bunts;
    }
	}
}
